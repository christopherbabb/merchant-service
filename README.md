Each Repo has its own Dockerfile for its respective app. K8 files are located in the K8 folder in the merchant-service project.

First I realized there was some build errors (an undeclared getter) I fixed. Then I figured out how to build and define a Dockerfile for a Spring-Boot app, having never worked with Java before.

Then I pushed the images to Docker Hub and spun up a local Kind cluster, then defined some simple K8 Deployments for the two services.

Then I defined two Services of type ClusterIP. The guide says both services shouldn't be exposed outside the cluster? So i'm guessing there should be some kind of ingress handling external requests? I'm assuming requests are routed to the Merchant Service, which then connects to the Shopping Service. 

Still have some networking configuration to play with.
